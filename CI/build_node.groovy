node {

        stage('Cleanup Workspace') {
            step([$class: 'WsCleanup'])
        }
        
        stage('Checkout Bilkalive-Backend Repository') {
            dir('bilkalive-backend') {
                def scmBackend = checkout([$class: 'GitSCM',
                    branches: [[name: env.bilkalive_backend_commit_id]],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: 'ebfd7687-112c-4870-ac3a-aefcd8cddbab',
                    url: 'git@bitbucket.org:DanskSupermarked/bilkalive-backend.git']]])
                env.GIT_COMMIT = scmBackend.GIT_COMMIT
            }
        }

        stage('Building version.txt for BilkaliveBackend') {
            sh 'echo DEPLOY_TIME=$BUILD_TIMESTAMP > bilkalive-backend/version.txt'
            sh 'echo BUILD_URL=$BUILD_URL >> bilkalive-backend/version.txt'
            sh 'echo "*****************************" >> bilkalive-backend/version.txt'
            sh 'echo "BilkaLiveBackend:" >> bilkalive-backend/version.txt'
            sh 'echo GIT_COMMIT=$GIT_COMMIT >> bilkalive-backend/version.txt'
        }
        stage('Build Container and run DB Migrations') {
            sh 'cp bilkalive-backend/CI/Dockerfile .'
            sh 'bilkalive-backend/CI/build_container_aws.sh -n d-BilkaLive -e d -i ${BUILD_NUMBER}'
        }
        stage('Create a new task definition and update ECS bifrost service') {
            sh 'cd bilkalive-backend/CI;./update_service.sh -e d -i ${BUILD_NUMBER}'
        }
}
