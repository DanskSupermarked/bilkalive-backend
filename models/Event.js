'use strict';
module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define('Event', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: DataTypes.TEXT,
    participantsLimit: DataTypes.INTEGER,
    slug: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {});

  Event.addHook('beforeValidate', (event, options) => {
    let self = event
    self.slug = self.title.split(' ').join('-').toLowerCase()
  })

  Event.associate = function(models) {
    // associations can be defined here
    Event.hasMany(models.Registration, {
      foreignKey: 'eventId',
      as: 'Registrations'
    })

    Event.belongsToMany(models.Field, {
      through: 'EventField',
      foreignKey: 'eventId',
      //otherKey: 'fieldId',
      //as: 'fields'
    })
  }
  
  return Event
}