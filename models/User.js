'use strict';
module.exports = (sequelize, DataTypes) => {
  const bcrypt = require('bcrypt');

  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    password: DataTypes.STRING
  }, {});

  User.addHook('beforeSave', async (user, options) => {
    let self = user
    const hash = await bcrypt.hash(self.password, 10)
    self.password = hash
  })

  User.prototype.isValidPassword = async function(password){
    const user = this;
    //Hashes the password sent by the user for login and checks if the hashed password stored in the 
    //database matches the one sent. Returns true if it does else false.
    const compare = await bcrypt.compare(password, user.password);
    return compare;
  }

  User.associate = function(models) {
    // associations can be defined here
  };

  return User;
};