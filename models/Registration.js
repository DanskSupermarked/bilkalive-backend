'use strict';
module.exports = (sequelize, DataTypes) => {
  const Registration = sequelize.define('Registration', {
    eventId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    formData: DataTypes.JSON
  }, {});

  Registration.associate = function(models) {
    Registration.belongsTo(models.Event, {
      foreignKey: 'eventId',
      onDelete: 'CASCADE'
    })
    // associations can be defined here
  };
  
  return Registration;
};