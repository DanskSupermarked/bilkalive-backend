'use strict';
module.exports = (sequelize, DataTypes) => {
  const Field = sequelize.define('Field', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: DataTypes.TEXT
  }, {})

  Field.associate = function(models) {
    // associations can be defined here
    Field.belongsToMany(models.Event, {
      through: 'EventField',
      //as: 'Events',
      foreignKey: 'fieldId'
    })
  }

  return Field;
};