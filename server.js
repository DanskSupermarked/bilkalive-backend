require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport')
const morgan = require('morgan')
//const db = require('./config/db')
const app = express()
const port = process.env.SERVER_PORT

// Morgan Route middleware that will happen on every request
app.use(morgan('tiny'))

require('./auth/auth')

// Parse incoming requests data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

// Allow CORS
app.use(function (req, res, next) {
    // TODO: this block should be tuned later
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

// Routes
const routes = require('./routes/routes')
const secureRoutes = require('./routes/secure-routes')

app.use('/', routes)
// We plugin our jwt strategy as a middleware so only verified users can access this route
app.use('/admin', passport.authenticate('jwt', {session: false}), secureRoutes)

// Handle errors
app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.json({error: err})
});

// Start server
app.listen(port, () => {
    console.log('We are live on ' + port)
})