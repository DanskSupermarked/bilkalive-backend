'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert('Users', [{
     email: 'admin@sprintingsoftware.com',
     password: 'password',
     createdAt: new Date(),
     updatedAt: new Date(),
   }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
