'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert('Events', [{
    title: 'Mardi Gras',
    description: "\'Mardi Gras\' means \'Fat Tuesday.\' Traditionally, it is the last day for Catholics to indulge—and often overindulge—before Ash Wednesday starts the sober weeks of fasting that come with Lent. Formally known as Shrove Tuesday, Mardi Gras has long been a time of extravagant fun for European Christians. In fact, some people think Mardi Gras celebrations have their source in the wild springtime orgies of the ancient Romans. In the United States, Mardi Gras draws millions of fun-seekers to New Orleans every year. Mardi Gras has been celebrated in New Orleans on a grand scale, with masked balls and colorful parades, since French settlers arrived in the early 1700s. Hidden behind masks, people behaved so raucously that for decades in the early 19th century masks were deemed illegal in that party-loving city.",
    participantsLimit: 100,
    slug: 'margdi-gras',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    title: 'New Year\'s Day',
    description: "The celebration of the new year on January 1st is a relatively new phenomenon. The earliest recording of a new year celebration is believed to have been in Mesopotamia, c. 2000 B.C. and was celebrated around the time of the vernal equinox, in mid-March. A variety of other dates tied to the seasons were also used by various ancient cultures. The Egyptians, Phoenicians, and Persians began their new year with the fall equinox, and the Greeks celebrated it on the winter solstice.",
    participantsLimit: 150,
    slug: 'new-years-day',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    title: 'St. Patrick\'s Day',
    description: "St. Patrick's Day celebrates the life of St. Patrick, the patron saint of Ireland who converted the Irish to Christianity in the 400s CE. By extension, the holiday is a celebration of Ireland, its people, and its history. This solemn holy day has spread and changed across the world. Learn more about the history and observation of the holiday, and the nation that inspires it.",
    participantsLimit: 80,
    slug: 'st-patricks-day',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {});
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('Events', null, {});
  }
};
