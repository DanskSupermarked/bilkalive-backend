const express = require('express')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const router = express.Router()

const {Event, Registration, User, Field} = require('../models/index')

// POST /signup # registration
router.post('/signup', passport.authenticate('signup', {session: false}, null), async (req, res, next) => {
    res.json({
        message: 'Signup successful',
        user: req.user
    });
})

// POST /login # authenticate
router.post('/login', async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) {
                const error = new Error('An Error occured')
                return next(error)
            }
            req.login(user, {session: false}, async (error) => {
                if (error) return next(error)
                // We don't want to store the sensitive information such as the
                // user password in the token so we pick only the email and id
                const body = {_id: user._id, email: user.email}
                // Sign the JWT token and populate the payload with the user email and id
                const token = jwt.sign({user: body}, 'top_secret')
                // Send back the token to the user
                return res.json({token})
            });
        } catch (error) {
            return next(error)
        }
    })(req, res, next)
});

// GET /
router.get('/', (req, res) => {
    res.send({name: 'Bilkalive API', version: '0.2'})
})

// GET /stats
router.get('/stats', (req, res) => {    

    let stats = {}

    stats.events = Event.count().then(res => {return res})
    stats.registrations = Registration.count()
    stats.fields = Field.count()

    Promise.all([stats.events, stats.registrations, stats.fields]).then(value => {
        stats.events = value[0]
        stats.registrations = value[1]
        stats.fields = value[2]
        res.status(200).send(stats)
    }).catch(err => {
        res.status(500).send(err)
    })
})

// GET /events
router.get('/events', (req, res) => {
    Event.findAll({
        include: [{
            model: Field,
            as: 'Fields',
            attributes: ['id', 'name']
        }]
    })
    .then((items, err) => {
        if (err) throw err
        res.send(items)
    })
})

// GET /events/:slug
router.get('/events/:slug', (req, res) => {
    const slug = req.params.slug
    const find_details = {slug}

    Event.findOne({
        where: find_details,
        include: ['Registrations', {
            model: Field,
            as: 'Fields',
            attributes: ['id', 'name']
        }]
    })
    .then((data) => {
        if (data == null) {
            res.status(404).send('The event with given :slug was not found.')
        } else {
            res.send(data)
        }
    })
    .catch((err) => {
        res.send(err)
    })
})

// PUT /events/:slug
router.put('/events/:slug', (req, res, next) => {
    const slug = req.params.slug
    const find_details = {slug}
    const update_details = req.body
    const update_options = {where: find_details, returning: true, runValidators: true}

    Event.update(
        update_details,
        update_options
    )
    .then((data) => {
        res.send(data)
    })
    .catch(next)
})

// DELETE /events/:slug
router.delete('/events/:slug', (req, res) => {
    const slug = req.params.slug
    const find_details = {slug}

    Event.findOne({where: find_details})
    .then((data) => {
        if (data === null) {
            res.status(404).send('The event with given :slug was not found.')
        } else {
            data.destroy()
            res.send({ message: 'Event deleted', id: data.id });
        }
    })
    .catch((err) => {
        res.send(err)
    })
})

// POST /events
router.post('/events', (req, res) => {
    Event.create(req.body)
    .then((data) => {
        res.status(200).send(data)
    })
    .catch((err) => {
        res.status(422).send(err)
    })
})

// GET /registrations
router.get('/registrations', (req, res) => {
    Registration.findAll({
        // include: [{
        //     model: Field,
        //     as: 'Fields',
        //     attributes: ['id', 'name']
        // }]
    })
    .then((items, err) => {
        if (err) throw err
        res.send(items)
    })
})

// POST /events/:slug/registrations
router.post('/events/:slug/registrations', (req, res) => {
    const slug = req.params.slug
    const find_details = {slug}

    Event.findOne({where: find_details})
    .then((data) => {
        if (data == null) {
            res.status(404).send('The event with given :slug was not found.')
        } else {
            Registration.create(
                {
                    eventId: data.id,
                    formData: req.body
                }
            ).then((data) => {
                console.log('Saved')
                res.send(data)
            })
            .catch((err) => {
                console.log('Error saving')
                res.send(err)
            })
        }
    })
})

// GET /events/:slug/fields
router.get('/events/:slug/fields', (req, res) => {
    const slug = req.params.slug
    const find_details = {slug}

    Event.findOne({
        where: find_details,
        include: [Field]
    })
    .then((data) => {
        res.send(data)
    }).catch((err) => {
        res.send(err)
    })
})

// GET /fields
router.get('/fields', (req, res) => {
    Field.findAll()
    .then((items, err) => {
        if (err) throw err
        res.send(items)
    })
})

// POST /fields
router.post('/fields', (req, res) => {
    Field.create(req.body)
    .then((data) => {
        res.status(200).send(data)
    })
    .catch((err) => {
        res.status(422).send(err)
    })
})

// POST /events/:slug/fields
router.get('/events/:slug/fields', (req, res) => {
    const slug = req.params.slug
    const find_details = {slug}

    Event.findOne({
        where: find_details
    })
    .then(data => {
        res.send(req.body)
        //return data.addField(req.body.Field)
        //res.send(data)
    }).catch((err) => {
        res.send(err)
    })
})

module.exports = router